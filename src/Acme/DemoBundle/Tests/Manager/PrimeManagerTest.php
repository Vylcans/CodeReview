<?php
namespace Acme\DemoBundle\Tests\Manager;
use Acme\DemoBundle\Manager\PrimeManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PrimeManagerTest extends WebTestCase
{
    public function testIndex()
    {
       $primeManager = new PrimeManager;
	   $result = $primeManager->selectOnlyPrimes( range( 1, 1000 ) );
	   $this->assertEquals(168, count($result));
	   
    }
}